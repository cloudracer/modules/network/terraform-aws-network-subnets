# Routing Subnets via Transit Gateway

locals {
  list_of_tgw_routes = { for route in setproduct(aws_route_table.this.*.id, var.subnets_routes_to_tgw) : md5("${route[0]}_${route[1]}") => route } # Need hash to know the key and not mix up entries in a list
  # Example: list_of_tgw_routes = [["rtb-x", "8.8.8.8/32"], ["rtb-y", "8.8.8.8/32"]]
}

resource "aws_route" "transit_gateway_routing" {
  for_each = length(var.subnets_routes_to_tgw) > 0 && length(var.subnets_cidrs) > 0 ? local.list_of_tgw_routes : {}

  route_table_id         = each.value[0] # The TRB ID
  destination_cidr_block = each.value[1] # The CIDR Range
  transit_gateway_id     = var.transit_gateway_id
}
