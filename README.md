# AWS VPC Subnets Moudle

## Overview

Creates Subnets from a List that looks like this:

- Map of Subnet Group
  - constructed Name of Group:
    - type: <public/private/protected>
    - cidrs: [List of CIDRs]
    - dedicated_network_acl:
      - inbound_acl_rules:
      - outbound_acl_rules:

```json
{
  "main_private" = {
    "name" = "main"
    "type" = "private"
    "cidrs" = [
      "10.10.10.0/24",
      "10.10.11.0/24",
    ]
    "dedicated_network_acl" = {
      "inbound_acl_rules" = [
        {
          "cidr_block" = "0.0.0.0/0"
          "from_port" = 0
          "protocol" = "-1"
          "rule_action" = "allow"
          "rule_number" = 100
          "to_port" = 0
        },
        {
          "cidr_block" = "192.0.0.0/8"
          "from_port" = 80
          "protocol" = "-1"
          "rule_action" = "deny"
          "rule_number" = 200
          "to_port" = 80
        },
      ]
      "outbound_acl_rules" = [
        {
          "cidr_block" = "0.0.0.0/0"
          "from_port" = 0
          "protocol" = "-1"
          "rule_action" = "allow"
          "rule_number" = 100
          "to_port" = 0
        },
      ]
    }
  }
}
```

## Usage

````
module "vpc_subnets_non_private" {
  source   = "./modules/subnets/"
  for_each = local.non_private_subnets_to_map

  # General info #
  vpc_id   = aws_vpc.this[0].id
  vpc_name = var.name

  # Gateway Info #
  route_to_igw            = var.create_vpc_internet_gateway
  igw_id                  = var.create_vpc_internet_gateway ? aws_internet_gateway.this.0.id : ""
  create_vpc_nat_gateways = var.create_vpc_nat_gateways

  # Info from Subnet YAML #
  subnet_name           = each.value.name
  subnets_cidrs         = each.value.cidrs
  subnet_type           = each.value.type
  dedicated_network_acl = lookup(each.value, "dedicated_network_acl", false) != false ? true : false
  inbound_acl_rules     = lookup(each.value, "dedicated_network_acl", false) != false ? lookup(each.value.dedicated_network_acl, "inbound_acl_rules", []) : []
  outbound_acl_rules    = lookup(each.value, "dedicated_network_acl", false) != false ? lookup(each.value.dedicated_network_acl, "outbound_acl_rules", []) : []

  # Other #
  tags = local.tags
}
````

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.22.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_eip.nat](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip) | resource |
| [aws_nat_gateway.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/nat_gateway) | resource |
| [aws_network_acl.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl) | resource |
| [aws_network_acl_rule.inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl_rule) | resource |
| [aws_network_acl_rule.outbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl_rule) | resource |
| [aws_ram_resource_association.subnets](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ram_resource_association) | resource |
| [aws_route.private_nat_gateway](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.public_internet_gateway](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.transit_gateway_routing](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route53_record.apex](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_route53_record.wildcard](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_route53_zone.zone](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone) | resource |
| [aws_route_table.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table_association.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_security_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_subnet.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_vpc_endpoint.dynamodbgateway](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint.s3gateway](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint_route_table_association.private_dynamodb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint_route_table_association) | resource |
| [aws_vpc_endpoint_route_table_association.private_s3](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint_route_table_association) | resource |
| [aws_availability_zones.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones) | data source |
| [aws_nat_gateway.nats](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/nat_gateway) | data source |
| [aws_subnet_ids.public](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet_ids) | data source |
| [aws_vpc_endpoint_service.dynamodbgateway](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc_endpoint_service) | data source |
| [aws_vpc_endpoint_service.s3gateway](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc_endpoint_service) | data source |
| [aws_vpc_endpoint_service.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc_endpoint_service) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_create_public_dns_name"></a> [create\_public\_dns\_name](#input\_create\_public\_dns\_name) | List of VPC Endpoints for which a public DNS name should be created | `list(string)` | `[]` | no |
| <a name="input_create_vpc_nat_gateways"></a> [create\_vpc\_nat\_gateways](#input\_create\_vpc\_nat\_gateways) | Wheter to create NAT gateways or not | `bool` | n/a | yes |
| <a name="input_dedicated_network_acl"></a> [dedicated\_network\_acl](#input\_dedicated\_network\_acl) | Whether to use dedicated network ACL (not default) and custom rules for subnets | `bool` | `false` | no |
| <a name="input_enable_endpoints"></a> [enable\_endpoints](#input\_enable\_endpoints) | List of VPC Endpoints that should be created | `list(string)` | `[]` | no |
| <a name="input_enable_ep_private_dns"></a> [enable\_ep\_private\_dns](#input\_enable\_ep\_private\_dns) | enable private dns for vpc endpouints | `bool` | `true` | no |
| <a name="input_igw_id"></a> [igw\_id](#input\_igw\_id) | ID of the IGW. (can be empty) | `string` | `""` | no |
| <a name="input_inbound_acl_rules"></a> [inbound\_acl\_rules](#input\_inbound\_acl\_rules) | Public subnets inbound network ACLs | `list(map(string))` | `[]` | no |
| <a name="input_nat_gateways_ids"></a> [nat\_gateways\_ids](#input\_nat\_gateways\_ids) | A List of NAT Gateway IDs (if present, otherwise the script will fetch them per data resource) | `list` | `[]` | no |
| <a name="input_outbound_acl_rules"></a> [outbound\_acl\_rules](#input\_outbound\_acl\_rules) | Public subnets outbound network ACLs | `list(map(string))` | `[]` | no |
| <a name="input_propagating_vgws"></a> [propagating\_vgws](#input\_propagating\_vgws) | vgws that should propagate into the route tables | `list(string)` | `[]` | no |
| <a name="input_ram_share_arn"></a> [ram\_share\_arn](#input\_ram\_share\_arn) | ARN of a RAM Resource share to which the subnets should be added | `string` | `""` | no |
| <a name="input_route_to_igw"></a> [route\_to\_igw](#input\_route\_to\_igw) | Wheter an IGW should be attached or not | `bool` | n/a | yes |
| <a name="input_share_this"></a> [share\_this](#input\_share\_this) | Whether to share these subnets | `bool` | `true` | no |
| <a name="input_subnet_name"></a> [subnet\_name](#input\_subnet\_name) | Name for the Subnets. (optional) | `string` | `""` | no |
| <a name="input_subnet_type"></a> [subnet\_type](#input\_subnet\_type) | Type of the subnet. Could be one of: public/private/protected | `string` | n/a | yes |
| <a name="input_subnets_cidrs"></a> [subnets\_cidrs](#input\_subnets\_cidrs) | A list of the Subnets CIDR Ranges | `list(string)` | n/a | yes |
| <a name="input_subnets_routes_to_tgw"></a> [subnets\_routes\_to\_tgw](#input\_subnets\_routes\_to\_tgw) | A list of CIDR Ranges that should be routet to a TGW | `list(string)` | `[]` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags that should be applied to all resources | `any` | n/a | yes |
| <a name="input_transit_gateway_id"></a> [transit\_gateway\_id](#input\_transit\_gateway\_id) | Transit Gateway ID for Routing | `string` | `""` | no |
| <a name="input_transit_gateway_routing"></a> [transit\_gateway\_routing](#input\_transit\_gateway\_routing) | If TGW Routing should be created or not | `bool` | `false` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | Reference to the Main VPC ID of the Subnets | `any` | n/a | yes |
| <a name="input_vpc_name"></a> [vpc\_name](#input\_vpc\_name) | Reference to the Main VPC Name of the Subnets | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_nat_gateways_ids"></a> [nat\_gateways\_ids](#output\_nat\_gateways\_ids) | IDs of the NATs created (if any, otherwise empty) |
| <a name="output_rtb_ids"></a> [rtb\_ids](#output\_rtb\_ids) | A list of all route tables of these subnets |
| <a name="output_subnet_id_cidr_map"></a> [subnet\_id\_cidr\_map](#output\_subnet\_id\_cidr\_map) | A Map of ID:CIDR for Subnets |
| <a name="output_subnet_ids"></a> [subnet\_ids](#output\_subnet\_ids) | IDs of the Subnets created (if any, otherwise empty) |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
