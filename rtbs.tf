# Route Tables & Association
resource "aws_route_table" "this" {
  count = length(var.subnets_cidrs) > 0 ? var.subnet_type == "private" ? length(var.subnets_cidrs) : 1 : 0 # For Type Public / Protected: 1 RTB for Private: x RTBs

  vpc_id = var.vpc_id

  propagating_vgws = var.propagating_vgws

  tags = merge(var.tags, {
    Name = lower("rtb-${var.vpc_name}-${var.subnet_name != "" ? var.subnet_name : "main"}-${var.subnet_type}-${var.subnet_type == "private" ? element(data.aws_availability_zones.current.names, count.index) : "all"}-tflz")
  })

  depends_on = [aws_subnet.this]
}

resource "aws_route_table_association" "this" {
  count = length(var.subnets_cidrs) > 0 ? length(var.subnets_cidrs) : 0

  subnet_id      = element(aws_subnet.this.*.id, count.index)
  route_table_id = var.subnet_type == "private" ? element(aws_route_table.this.*.id, count.index) : aws_route_table.this.0.id
}
