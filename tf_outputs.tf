output "nat_gateways_ids" {
  value       = aws_nat_gateway.this.*.id
  description = "IDs of the NATs created (if any, otherwise empty)"
}

output "subnet_ids" {
  value       = aws_subnet.this.*.id
  description = "IDs of the Subnets created (if any, otherwise empty)"
}

locals {
  subnet_id_cidr_map = { for subnet in aws_subnet.this.* : subnet.id => subnet.cidr_block }
}

output "subnet_id_cidr_map" {
  value       = local.subnet_id_cidr_map
  description = "A Map of ID:CIDR for Subnets"
}

output "rtb_ids" {
  value       = aws_route_table.this.*.id
  description = "A list of all route tables of these subnets"
}
