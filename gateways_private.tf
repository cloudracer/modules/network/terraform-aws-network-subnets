# NAT Route Entry for Private Subnets
# (This is a little bit complex, as the Module has an isolate)

## Find Main Public Subnets
data "aws_subnet_ids" "public" {
  count = var.create_vpc_nat_gateways && var.subnet_type == "private" && length(var.nat_gateways_ids) <= 0 ? 1 : 0

  vpc_id = var.vpc_id

  filter {
    name   = "tag:Name"
    values = [lower("sub-${var.vpc_name}-main-public-*-tflz")] # Find the main public VPCs
  }
}

## Find NATs in Public Subnets
data "aws_nat_gateway" "nats" {
  count = var.create_vpc_nat_gateways && var.subnet_type == "private" && length(var.nat_gateways_ids) <= 0 ? length(data.aws_subnet_ids.public) : 0

  subnet_id = tolist(data.aws_subnet_ids.public[0].ids)[count.index]
}

resource "aws_route" "private_nat_gateway" {
  count = var.create_vpc_nat_gateways && var.subnet_type == "private" ? length(var.subnets_cidrs) : 0

  route_table_id         = element(aws_route_table.this.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = length(var.nat_gateways_ids) > 0 ? element(var.nat_gateways_ids, count.index) : element(data.aws_nat_gateway.nats.*.id, count.index)

  timeouts {
    create = "5m"
  }
}
