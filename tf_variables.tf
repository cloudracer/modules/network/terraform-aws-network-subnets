# General info #
variable "vpc_id" {
  description = "Reference to the Main VPC ID of the Subnets"
}

variable "vpc_name" {
  description = "Reference to the Main VPC Name of the Subnets"
}

# RAM Share #
variable "ram_share_arn" {
  description = "ARN of a RAM Resource share to which the subnets should be added"
  default     = ""
}

# Info from Subnet YAML #
variable "subnet_name" {
  type        = string
  description = "Name for the Subnets. (optional)"
  default     = ""
}

variable "subnets_cidrs" {
  type        = list(string)
  description = "A list of the Subnets CIDR Ranges"
}

variable "subnet_type" {
  type        = string
  description = "Type of the subnet. Could be one of: public/private/protected"
}

variable "subnets_routes_to_tgw" {
  type        = list(string)
  description = "A list of CIDR Ranges that should be routet to a TGW"
  default     = []
}

# Gateway Info #
variable "route_to_igw" {
  type        = bool
  description = "Wheter an IGW should be attached or not"
}

variable "igw_id" {
  description = "ID of the IGW. (can be empty)"
  default     = ""
}

variable "create_vpc_nat_gateways" {
  type        = bool
  description = "Wheter to create NAT gateways or not"
}

variable "nat_gateways_ids" {
  description = "A List of NAT Gateway IDs (if present, otherwise the script will fetch them per data resource)"
  default     = []
}

variable "transit_gateway_routing" {
  type        = bool
  description = "If TGW Routing should be created or not"
  default     = false
}

variable "transit_gateway_id" {
  type        = string
  description = "Transit Gateway ID for Routing"
  default     = ""
}


# ACLs
variable "dedicated_network_acl" {
  description = "Whether to use dedicated network ACL (not default) and custom rules for subnets"
  type        = bool
  default     = false
}

variable "inbound_acl_rules" {
  description = "Public subnets inbound network ACLs"
  type        = list(map(string))
  default     = []
}

variable "outbound_acl_rules" {
  description = "Public subnets outbound network ACLs"
  type        = list(map(string))
  default     = []
}

# Endpoints #
variable "enable_endpoints" {
  description = "List of VPC Endpoints that should be created"
  type        = list(string)
  default     = []
}

variable "enable_ep_private_dns" {
  description = "enable private dns for vpc endpouints"
  default     = true
  type        = bool
}

variable "create_public_dns_name" {
  description = "List of VPC Endpoints for which a public DNS name should be created"
  type        = list(string)
  default     = []
}

# Other #
variable "tags" {
  description = "Tags that should be applied to all resources"
}

variable "share_this" {
  type        = bool
  description = "Whether to share these subnets"
  default     = true
}

variable "propagating_vgws" {
  type        = list(string)
  description = "vgws that should propagate into the route tables"
  default     = []
}
