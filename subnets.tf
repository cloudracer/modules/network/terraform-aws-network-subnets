# Subnet Creation
resource "aws_subnet" "this" {
  count = length(var.subnets_cidrs) > 0 ? length(var.subnets_cidrs) : 0

  vpc_id                  = var.vpc_id
  availability_zone       = element(data.aws_availability_zones.current.names, count.index)
  cidr_block              = element(var.subnets_cidrs, count.index)
  map_public_ip_on_launch = var.subnet_type == "public" ? true : false

  tags = merge(var.tags, {
    Name = lower("sub-${var.vpc_name}-${var.subnet_name != "" ? var.subnet_name : "main"}-${var.subnet_type}-${element(data.aws_availability_zones.current.names, count.index)}-tflz")
  })
}
