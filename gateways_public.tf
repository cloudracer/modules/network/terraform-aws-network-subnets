# Gateways for Public Subnets

## Internet Gateway Route for Public Subnets
resource "aws_route" "public_internet_gateway" {
  count = var.subnet_type == "public" && var.route_to_igw == true ? 1 : 0

  route_table_id         = aws_route_table.this.0.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = var.igw_id

  depends_on = [aws_route_table.this]
}

## NAT Gateways for Public Subnets
resource "aws_eip" "nat" {
  count = var.create_vpc_nat_gateways && var.subnet_type == "public" && var.subnet_name == "main" ? length(var.subnets_cidrs) : 0

  vpc = true

  tags = merge(var.tags, {
    Name = lower("eip-${var.vpc_name}-nat-${element(data.aws_availability_zones.current.names, count.index)}-tflz")
  })
}

resource "aws_nat_gateway" "this" {
  count = var.create_vpc_nat_gateways && var.subnet_type == "public" && var.subnet_name == "main" ? length(var.subnets_cidrs) : 0

  allocation_id = element(aws_eip.nat.*.id, count.index)
  subnet_id     = element(aws_subnet.this.*.id, count.index)

  tags = merge(var.tags, {
    Name = lower("nat-${var.vpc_name}-${element(data.aws_availability_zones.current.names, count.index)}-tflz")
  })
}
