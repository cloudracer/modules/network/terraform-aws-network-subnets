/////////////////////////
// interface endpoints //
/////////////////////////
data "aws_vpc_endpoint_service" "this" {
  for_each = toset(local.var_endpoint_setup)

  service      = each.value
  service_type = "Interface"
}

resource "aws_vpc_endpoint" "this" {
  for_each = toset(local.var_endpoint_setup)

  vpc_id              = var.vpc_id
  service_name        = data.aws_vpc_endpoint_service.this[each.value].service_name
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = each.key == "s3" ? false : var.enable_ep_private_dns

  security_group_ids = [aws_security_group.this[each.key].id]
  subnet_ids         = aws_subnet.this.*.id
  tags               = merge(var.tags, local.tags, { Name = "endpoint-${replace(replace(lower(each.value), "-", ""), ".", "")}-${var.vpc_name}-tflz" })
}

/////////////////////
// security groups //
/////////////////////
resource "aws_security_group" "this" {
  for_each    = toset(local.var_endpoint_setup)
  name        = "endpoint-sg-${replace(replace(lower(each.value), "-", ""), ".", "")}-${var.vpc_name}-tflz"
  description = "Endpoint SG, allows all"
  vpc_id      = var.vpc_id

  ingress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = merge(var.tags, local.tags, { Name = "sg-endpoint-${replace(replace(lower(each.value), "-", ""), ".", "")}-${var.vpc_name}-tflz" })
}

///////////////////////
// gateway endpoints //
///////////////////////
//s3
data "aws_vpc_endpoint_service" "s3gateway" {
  count = contains(local.var_enable_endpoints_parsed, "s3gateway") ? 1 : 0

  service      = "s3"
  service_type = "Gateway"
}

resource "aws_vpc_endpoint" "s3gateway" {
  count = contains(local.var_enable_endpoints_parsed, "s3gateway") ? 1 : 0

  vpc_id       = var.vpc_id
  service_name = data.aws_vpc_endpoint_service.s3gateway[0].service_name
  tags         = merge(var.tags, local.tags, { Name = "endpoint-s3gateway-${var.vpc_name}-tflz" })
}

resource "aws_vpc_endpoint_route_table_association" "private_s3" {
  count = contains(local.var_enable_endpoints_parsed, "s3gateway") ? length(aws_route_table.this) : 0

  vpc_endpoint_id = aws_vpc_endpoint.s3gateway[0].id
  route_table_id  = element(aws_route_table.this.*.id, count.index)
}

//dynamodb
data "aws_vpc_endpoint_service" "dynamodbgateway" {
  count = contains(local.var_enable_endpoints_parsed, "dynamodbgateway") ? 1 : 0

  service      = "dynamodb"
  service_type = "Gateway"
}

resource "aws_vpc_endpoint" "dynamodbgateway" {
  count = contains(local.var_enable_endpoints_parsed, "dynamodbgateway") ? 1 : 0

  vpc_id       = var.vpc_id
  service_name = data.aws_vpc_endpoint_service.dynamodbgateway[0].service_name
  tags         = merge(var.tags, local.tags, { Name = "endpoint-dynamodbgateway-${var.vpc_name}-tflz" })
}

resource "aws_vpc_endpoint_route_table_association" "private_dynamodb" {
  count = contains(local.var_enable_endpoints_parsed, "dynamodbgateway") ? length(aws_route_table.this) : 0

  vpc_endpoint_id = aws_vpc_endpoint.dynamodbgateway[0].id
  route_table_id  = element(aws_route_table.this.*.id, count.index)
}

resource "aws_route53_zone" "zone" {
  for_each = toset(var.create_public_dns_name)
  name     = join(".", reverse(split(".", data.aws_vpc_endpoint_service.this[each.value].service_name)))

  vpc {
    vpc_id = var.vpc_id
  }
  tags = var.tags
}

# If you see spurious updates to the S3 entries with "*" in the alias:
# Don't worry https://github.com/hashicorp/terraform-provider-aws/issues/10843

resource "aws_route53_record" "apex" {
  for_each = toset(var.create_public_dns_name)
  zone_id  = aws_route53_zone.zone[each.value].zone_id
  name     = join(".", reverse(split(".", data.aws_vpc_endpoint_service.this[each.value].service_name)))
  type     = "A"
  alias {
    evaluate_target_health = true
    name                   = aws_vpc_endpoint.this[each.value].dns_entry[0].dns_name
    zone_id                = aws_vpc_endpoint.this[each.value].dns_entry[0].hosted_zone_id
  }
}

resource "aws_route53_record" "wildcard" {
  for_each = toset(var.create_public_dns_name)
  zone_id  = aws_route53_zone.zone[each.value].zone_id
  name     = "*.${join(".", reverse(split(".", data.aws_vpc_endpoint_service.this[each.value].service_name)))}"

  type = "A"
  alias {
    evaluate_target_health = true
    name                   = aws_vpc_endpoint.this[each.value].dns_entry[0].dns_name
    zone_id                = aws_vpc_endpoint.this[each.value].dns_entry[0].hosted_zone_id
  }
}
