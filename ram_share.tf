resource "aws_ram_resource_association" "subnets" {
  count = var.share_this ? length(var.subnets_cidrs) : 0

  resource_arn       = element(aws_subnet.this.*.arn, count.index)
  resource_share_arn = var.ram_share_arn
}
