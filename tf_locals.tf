locals {
  # var_enable_endpoints_parsed = [for endpoint in var.enable_endpoints : replace(replace(lower(endpoint), "-", ""), ".", "")]
  # var_security_groups         = compact([for endpoint in local.var_enable_endpoints_parsed : endpoint == "s3" || endpoint == "dynamodb" ? "" : endpoint])

  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-network-subnets"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/network/terraform-aws-network-subnets.git"
  }
  tags = merge(local.tags_module, var.tags)
}
